package com.heg.turntable.games

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.generic.Players
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.games.rules.generic.ReadyUp
import com.heg.turntable.games.rules.generic.Reset
import com.heg.turntable.games.rules.generic.TurnBased
import com.heg.turntable.games.rules.specific.BattleShips
import com.heg.turntable.games.rules.specific.ConnectFour
import com.heg.turntable.games.rules.specific.Foursies
import com.heg.turntable.games.rules.specific.Nonograms
import com.heg.turntable.games.rules.specific.NoughtsAndCrosses
import com.heg.turntable.games.rules.specific.chess.Chess
import com.heg.turntable.webserver.WebSocketException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.IndexOutOfBoundsException
import java.net.URLDecoder.decode
import java.nio.charset.StandardCharsets.UTF_8

class GameRegistry (
    private val mapper: ObjectMapper,
    private val registry: Map<String, List<Rule>> = mapOf(

        "battleships" to listOf(
            Players(2, mapper),
            ReadyUp(mapper, "shots"),
            BattleShips(mapper),
            TurnBased("shots"),
        ),

        "connect-four" to listOf(
            Players(2, mapper),
            TurnBased("board"),
            ConnectFour(mapper),
            Reset(
                mapper = mapper,
                resetKey = "board",
                resetValue = """
                    {
                        "0" : [],
                        "1" : [],
                        "2" : [],
                        "3" : [],
                        "4" : [],
                        "5" : [],
                        "6" : [],
                        "7" : [],
                        "8" : [],
                        "9" : [],
                        "10" : [],
                        "11" : [],
                        "12" : [],
                        "13" : [],
                        "14" : [],
                        "15" : []
                    }
                """
            )
        ),

        "nonograms" to listOf(
            Nonograms(mapper)
        ),

        "noughts-and-crosses" to listOf(
            Players(2, mapper),
            TurnBased("board"),
            NoughtsAndCrosses(mapper),
            Reset(
                mapper = mapper,
                resetKey = "board",
                resetValue = """
                    {
                        "topLeft": "", "topMid": "", "topRight": "",
                        "midLeft": "", "mid": "", "midRight": "",
                        "bottomLeft": "", "bottomMid": "", "bottomRight": ""
                    }
                """
            )
        ),

        "foursies" to listOf(
            Players(2, mapper),
            TurnBased("board"),
            Foursies(mapper),
            Reset(
                mapper = mapper,
                resetKey = "board",
                resetValue = """
                    [
                        [1, 1, 1, 1],
                        [0, 0, 0, 0],
                        [0, 0, 0, 0],
                        [2, 2, 2, 2]
                    ]
                """
            )
        ),

        "no-you" to listOf(
            Players(2, mapper),
            TurnBased()
        ),

        "chess" to listOf(
            Players(2, mapper,),
            TurnBased("board"),
            Chess(mapper),
            Reset(
                    mapper = mapper,
                    resetKey = "board",
                    resetValue = """
                        {A1}
                    """
            )
        )

    )
) {

    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    fun applyReadRules(gameJson: JsonNode, gameName: String, userId: String): JsonNode {
        log.info("Reading for $gameName")
        var newJson = gameJson
        for (rule in registry.getOrDefault(gameName, emptyList())) {
            newJson = rule.alterRead(newJson.deepCopy(), userId)
        }
        return newJson
    }

    fun applyWriteRules(gameJson: JsonNode, moveJson: String, gameName: String, userId: String): JsonNode {
        log.info("Applying Write rules for $gameName")
        registry.getOrDefault(gameName, emptyList())
        var newJson: ObjectNode = mapper.readTree(moveJson).deepCopy()
        for (rule in registry.getOrDefault(gameName, emptyList())) {
            newJson = rule.alterWrite(gameJson, newJson, userId)
        }
        return newJson
    }

    fun initialiseJson(initForm: String, gameName: String): JsonNode {
        log.info("Initialising $gameName game using $initForm")
        registry.getOrDefault(gameName, emptyList())
        var newJson: ObjectNode = jsonifyForm(initForm)
        for (rule in registry.getOrDefault(gameName, emptyList())) {
            newJson = rule.initialiseMap(newJson)
        }
        return newJson
    }

    private fun jsonifyForm(initForm: String): ObjectNode =
        try {
            mapper.valueToTree<ObjectNode?>(
                initForm
                    .split("&")
                    .map { form ->
                        form
                            .split("=")
                            .map {
                                decode(it, UTF_8)
                            }
                    }.associate { it[0] to it[1] }
            ).deepCopy()
        } catch (e: IndexOutOfBoundsException ) {
            // TODO: Not this
            mapper.readTree("{}").deepCopy()
        } catch (e: JsonParseException ) {
            throw WebSocketException("Unable to parse form data to json")
        }

}