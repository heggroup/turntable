package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.games.rules.utils.playerName
import com.heg.turntable.webserver.WebSocketException

class Reset (
    private val resetKey: String,
    private val resetValue: String,
    private val mapper: ObjectMapper
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        moveJson.putArray("resetVotes")
        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        if ( moveJson.has("resetVotes") ) {

            moveJson.withArray("resetVotes").forEach {
                if ( it.textValue() != playerName(originalJson, playerId)) {
                    throw WebSocketException("You cannot vote reset for another player")
                }
            }

            val combinedVotes = mapper.createArrayNode()

            (moveJson
                .withArray("resetVotes")
                .toSet()
                .plus(
                    originalJson
                        .withArray("resetVotes") as JsonNode)
                        .toSet()
                )
                .forEach { combinedVotes.add(it) }

            moveJson.put("resetVotes", combinedVotes)

            if (combinedVotes.size() == originalJson["players"].fields().asSequence().toList().size) {
                moveJson.put("resetVotes", mapper.createArrayNode())
                moveJson.put(resetKey, mapper.readTree(resetValue))
            }
        }
        return moveJson
    }
}