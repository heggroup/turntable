package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.webserver.WebSocketException

class TurnBased (
    private val areaName: String = "board"
        ): Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        if (!moveJson.has(areaName)) {
            moveJson.putNull(areaName)
        }
        moveJson.put("turn", 0)
        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        if (moveJson.has(areaName)) {
            if ( playerId != currentPlayer(originalJson) ) {
                throw WebSocketException("It is not your turn")
            } else {
                moveJson.put("turn", originalJson.get("turn").intValue() + 1)
            }
        }
        return moveJson
    }

    private fun currentPlayer(gameJson: JsonNode): String =
        gameJson["players"]
            .get(((gameJson["turn"].asInt() % gameJson["players"].size()) + 1).toString())
            .get("secretId")
            .textValue()
}