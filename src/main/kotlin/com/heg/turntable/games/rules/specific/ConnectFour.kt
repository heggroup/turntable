package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.webserver.WebSocketException

class ConnectFour(
    private val mapper: ObjectMapper
): Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        val board = mapper.createObjectNode()

        for (i in 0..moveJson["width"].textValue().toInt()) {
            board.put(i.toString(), mapper.createArrayNode())
        }

        moveJson.put("board", board)

        val colours = mapper.createObjectNode()
        colours.put("1", "#EE0000")
        colours.put("2", "#EEEE00")
        moveJson.put("colours", colours)

        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        val boardChanges = moveJson["board"]
        if (
            boardChanges != null &&
            boardChanges
                .fieldNames()
                .asSequence()
                .toList()
                .intersect(
                    (0..originalJson["width"].textValue().toInt()).map { it.toString() }.toSet()
                ).isNotEmpty()
        ) {
            if ( boardChanges.fieldNames().asSequence().toList().size > 1 ) {
                throw WebSocketException("You can only add one piece at a time")
            } else {
                val key = boardChanges.fields().asSequence().first().key
                if ( originalJson["board"][key].toList().size >= originalJson["height"].textValue().toInt()  ) {
                    throw WebSocketException("Column is full")
                } else {
                    val newColumn = mapper.createArrayNode()

                    originalJson["board"][key].forEach { newColumn.add(it.textValue()) }
                    newColumn.add(boardChanges[key])

                    val newBoard = mapper.createObjectNode()
                    newBoard.put(key, newColumn)
                    moveJson.put("board", newBoard)
                }
            }
        }
        return moveJson
    }

}