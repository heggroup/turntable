package com.heg.turntable.games.rules

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode

interface Rule {

    // This function is called when a new game is created. moveJson is the currently initialised JSON to which you
    // should add your changes

    fun initialiseMap(moveJson: ObjectNode): ObjectNode = moveJson

    // AlterRead is for editing the view of the game state. A typical use case would be removing secrets from the JSON
    // before they are sent to the user.

    fun alterRead(originalJson: ObjectNode, playerId: String = ""): JsonNode = originalJson

    // AlterWrite is for interpreting the move that a given player wishes to enact. Changes should be made to the
    // moveJson to reflect the interpretation of the rules. The updated moveJson should then be returned so that it can
    // then be interpreted by the other rules before being merged with originalJson.

    fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode = moveJson

}