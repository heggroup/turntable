package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.games.rules.utils.playerIndex
import com.heg.turntable.webserver.WebSocketException

class ReadyUp(
    private val mapper: ObjectMapper,
    private val protectedKey: String
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        val readys = mapper.createObjectNode()
        for ( key in moveJson["players"].fieldNames() ) {
            readys.put(key, false)
        }
        moveJson.put("readys", readys)
        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        if ( moveJson.has(protectedKey) ) {
            if ( !everyoneIsReady(originalJson) )
                throw WebSocketException("You must wait until all players are ready")
        }
        if ( moveJson.has("readys") ) {
            if ( everyoneIsReady(originalJson) ) {
                throw WebSocketException("You cannot change your ready status after all players have readied up")
            }
            if (
                moveJson["readys"]
                    .fieldNames()
                    .asSequence()
                    .toList()
                    .any { it != playerIndex(originalJson, playerId)  }
            ) {
                throw WebSocketException("You cannot ready up for another player")
            }
        }
        return moveJson
    }

    private fun everyoneIsReady(json: JsonNode): Boolean =
        json["readys"]
            .fields()
            .asSequence()
            .toList()
            .all{ it.value.booleanValue() }

}