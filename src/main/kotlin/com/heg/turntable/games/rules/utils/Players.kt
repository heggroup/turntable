package com.heg.turntable.games.rules.utils

import com.fasterxml.jackson.databind.JsonNode

fun playerIndex(json: JsonNode, id: String): String? {
    if ( json.has("players") ) {
        for (player in json["players"].fields()) {
            if (player.value.has("secretId") && player.value["secretId"].textValue() == id) {
                return player.key
            }
        }
    }
    return null
}

fun playerName(originalJson: JsonNode, playerId: String) : String {
    originalJson["players"].fields().forEach {
        if ( it.value.has("secretId") && it.value["secretId"].textValue() == playerId ) {
            return it.value["name"].textValue()
        }
    }
    return ""
}

fun gameIsFull(originalJson: JsonNode): Boolean =
    !originalJson["players"]
        .fields()
        .asSequence()
        .any { it.value.isNull || it.value["name"].isNull }

fun playersAreReady(originalJson: JsonNode): Boolean =
    originalJson["readys"]
        .fields()
        .asSequence()
        .all { it.value.booleanValue() }
