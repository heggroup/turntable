package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

private val mapper = ObjectMapper()

fun emptyGrid(width: Int, height: Int = width): ObjectNode {
    val grid = mapper.createObjectNode()
    for (x in 1..width) {
        val column = mapper.createObjectNode()
        for (y in 1..height) {
            column.put(y.toString(), "")
        }
        grid.put(x.toString(), column)
    }
    return grid
}