package com.heg.turntable.games.rules.specific.chess.pieces

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

class Pawn(
    private val mapper: ObjectMapper,
    private val isBlack: Boolean,
    private val x: Char,
    private val y: Int = if (isBlack) 7 else 2,
    private val hasMoved: Boolean = false
): Piece {
    private val name: String = "pawn"
    private val colour: String = if (isBlack) "black" else "white"

    override fun isValidMove(moveJson: ObjectNode): Boolean {
        TODO("Not yet implemented")

        return false
    }

    override fun toJson(): ObjectNode =
            mapper.readTree("""
            {
                "name": "$name",
                "colour": "$colour",
                "hasMoved": "$hasMoved"
            }
        """).deepCopy()
}