package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import com.heg.turntable.games.rules.generic.emptyGrid
import com.heg.turntable.games.rules.utils.gameIsFull
import com.heg.turntable.games.rules.utils.playerIndex
import com.heg.turntable.games.rules.utils.playersAreReady
import com.heg.turntable.webserver.WebSocketException

class BattleShips (
    private val mapper: ObjectMapper
) : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {

        val size = moveJson["size"].textValue().toInt()

        val boats = mapper.createObjectNode()
        boats.put("1", emptyGrid(size))
        boats.put("2", emptyGrid(size))

        val shots = mapper.createObjectNode()
        shots.put("1", emptyGrid(size))
        shots.put("2", emptyGrid(size))

        moveJson.put("boats", boats)
        moveJson.put("shots", shots)

        return moveJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        if ( moveJson.has("shots")) {

            val playerIndex: String = moveJson["shots"].fieldNames().asSequence().first()
            val x = moveJson["shots"][playerIndex].fieldNames().asSequence().first()
            val y = moveJson["shots"][playerIndex][x].fieldNames().asSequence().first()

            val shots: ObjectNode = originalJson["shots"].deepCopy()
            val playerShots: ObjectNode = shots[playerIndex].deepCopy()
            val shotsX: ObjectNode = playerShots[x].deepCopy()

            val otherPlayerIndex =
                if (playerIndex == "1") {
                    "2"
                } else {
                    "1"
                }

            if (originalJson["boats"][otherPlayerIndex][x][y].textValue() == "") {
                shotsX.put(y, "MISS")
            } else {
                shotsX.put(y, "HIT")
            }

            playerShots.put(x, shotsX)
            shots.put(playerIndex, playerShots)
            moveJson.put("shots", shots)
        }
        if ( moveJson.has("boats") ) {
            if ( playersAreReady(originalJson) ) {
                throw WebSocketException("Cannot move ships once the game has started")
            }
            for ( xKey: String in moveJson["boats"][playerIndex(originalJson, playerId)].fieldNames() ) {
                for ( yKey in moveJson["boats"][playerIndex(originalJson, playerId)][xKey].fieldNames() ) {
                    if ( originalJson["boats"][playerIndex(originalJson, playerId)][xKey][yKey].textValue() != "" ) {
                        throw WebSocketException("Ships cannot overlap")
                    }
                }
            }
        }
        return moveJson
    }

    override fun alterRead(originalJson: ObjectNode, playerId: String): JsonNode {
        val newBoats = mapper.createObjectNode()
        if ( gameIsFull(originalJson) ) {
            when ( playerIndex(originalJson, playerId) ) {
                "1" -> {
                    newBoats.put("1", originalJson["boats"]["1"])
                }
                "2" -> {
                    newBoats.put("2", originalJson["boats"]["2"])
                }
            }
        }
        originalJson.put("boats", newBoats)
        return originalJson
    }
}