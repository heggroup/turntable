package com.heg.turntable.games

import com.fasterxml.jackson.databind.JsonNode
import com.heg.turntable.datasource.MergingJsonStore
import org.slf4j.LoggerFactory
import java.util.concurrent.locks.ReentrantLock


class MoveExecutor(
    private val jsonStore: MergingJsonStore,
    private val gameRegistry: GameRegistry,
    private val multiLock: StringMultiLock = StringMultiLock()
) {

    fun write(data: String, userId: String, roomId: String, gameName: String) {

        try {
            multiLock.acquireLock(roomId)

            jsonStore.write(
                roomId,
                gameRegistry.applyWriteRules(
                    jsonStore.read(roomId),
                    data,
                    gameName,
                    userId
                )
            )
        }
        finally {
            multiLock.releaseLock(roomId)
        }
    }

    fun read(roomId: String, gameName: String, userId: String): JsonNode =
        gameRegistry.applyReadRules(jsonStore.read(roomId), gameName, userId)

}

class StringMultiLock(
    private val masterLock: ReentrantLock = ReentrantLock(true),
    private val lockMap: MutableMap<String, ReentrantLock> = mutableMapOf()
) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    fun acquireLock(lockString: String) {
        if (!lockMap.containsKey(lockString)) {
            masterLock.lock()
            lockMap[lockString] = ReentrantLock(true)
            masterLock.unlock()
        }

        lockMap[lockString]!!.lock()
    }

    fun releaseLock(lockString: String) {
        val lock = lockMap[lockString]!!
        try {
            if (!lock.hasQueuedThreads()) {
                try {
                    masterLock.lock()
                    lockMap.remove(lockString)
                }
                finally {
                    masterLock.unlock()
                }
            } else {
                log.info("${lock.queueLength} requests waiting for $lockString lock")
            }
        }
        finally {
            lock.unlock()
        }

    }

}


