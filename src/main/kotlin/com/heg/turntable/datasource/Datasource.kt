package com.heg.turntable.datasource

interface Datasource {

    fun write(roomId: String, moveJson: String)

    fun read(roomId: String): String

    fun keyExists(roomId: String): Boolean
}