package com.heg.turntable.datasource

import redis.clients.jedis.JedisPool

class Redis(
    private val redisPool: JedisPool,
    private val expiryPeriod: Long
) :Datasource {

    override fun write(roomId: String, moveJson: String) {
        redisPool.resource.use {
            it.setex(roomId, expiryPeriod, moveJson)
        }
    }

    override fun read(roomId: String): String =
        redisPool.resource.use {
            if (it.exists(roomId)) {
                it.get(roomId)
            } else {
                "{}"
            }
        }

    override fun keyExists(roomId: String): Boolean =
        redisPool.resource.use {
            it.exists(roomId)
        }
}