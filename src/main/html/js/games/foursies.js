

const xCount = 4;
const yCount = 4;

var socket = null;
var gameJson = null;
var chosenSquare = null;

window.onload = function() {
    makeUid()
    startWebSocket()
}

function startWebSocket() {
    socket = new WebSocket(
        window.location.href
            .replace("http", "ws")
        + "/" + getUid()
    );

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    let errorDiv = document.getElementById("errorDiv")

    errorDiv.innerHTML = message
    errorDiv.style.visibility = "visible"

    setTimeout(function() {
        errorDiv.style.visibility = "hidden"
    }, 3000)
}

function renderJson(json) {
    if (userIsNotAPlayer(json)) {
        renderLoginForm(document.getElementById("mainDiv"))
    }
    else if (gameIsNotFull(json)) {
        mainDiv = document.getElementById("mainDiv").innerHTML =
            "Waiting for other player...";
    }
    else {
        renderGame(json)
        renderResetDiv(json)
    }
}

function renderGame(json) {
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }

    const vw = Math.max(
        document.documentElement.clientWidth || 0,
        window.innerWidth || 0
    );
    const vh = Math.max(
        document.documentElement.clientHeight || 0,
        window.innerHeight || 0
    );

    size = Math.min(vw, vh)
    document.getElementById("mainDiv").innerHTML = `
        <canvas id="board" width="${size}" height="${size}"></canvas>

    `
    canvas = document.getElementById("board")
    canvas.onmouseup = function(event) {
        const rect = canvas.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;
        onClick(x, y, canvas.height / xCount, canvas.height / yCount);
    }

    drawBoard(canvas);
    drawMarks(canvas, json);
    drawChosenSquare(canvas);

    const winner = checkWon(json);
    if (winner !== 0) {
        drawWin(canvas, json, winner - 1)
    }
}

function checkWon(json) {
    const board = json["board"];
    if (board[0].includes(2)) {
        return 2;
    } else if (board[3].includes(1)) {
        return 1;
    } else if (!includes(board, 1)) {
        return 2;
    } else if(!includes(board, 2)) {
        return 1;
    } else {
        return 0;
    }
}

function includes(board, num) {
    return board.some(row => row.includes(num));
}

function drawWin(canvas, json, winner) {
    canvas.onmouseup = null
    size = canvas.height
    f = canvas.getContext("2d")
    f.fillStyle = "rgba(255, 255, 255, 0.5)"
    f.rect(0, 0, size, size)
    f.fill()


    f.font = `bold ${size/10}px Arial`
    f.textBaseline = 'middle';
    f.textAlign = 'center';

    const winnerName = playerNames(json)[winner];

    f.fillStyle = "rgba(0, 0, 0)";
    f.fillText(winnerName + " Wins!", size / 2, size / 2)
}

function drawBoard(canvas) {
    size = canvas.height

    p = canvas.getContext("2d")
    p.beginPath();
    p.lineWidth = 1;
    p.strokeStyle = 'black';

    let xw = size / xCount;
    let yw = size / yCount;

    for (let x = 0; x < xCount + 1; x++) {
        p.moveTo(x * xw, 0);
        p.lineTo(x * xw, size);
    }

    for (let y = 0; y < yCount + 1; y++) {
        p.moveTo(0, y * yw)
        p.lineTo(size, y * yw)
    }

    p.stroke()
}

function drawMarks(canvas, json) {
    size = canvas.height

    let xw = size / xCount;
    let yw = size / yCount;

    f = canvas.getContext("2d")
    f.font = `bold ${size / xCount}px Arial`
    f.textBaseline = 'middle';
    f.textAlign = 'center';

    for (let x = 0; x < xCount; x++) {
        for (let y = 0; y < yCount; y++) {
            f.fillText(
                textForCode(json["board"][y][x]),
                (x + 0.5) * xw,
                (y + 0.5) * yw
            )
        }
    }
}

function drawChosenSquare(canvas) {
    if (chosenSquare === null) {
        return;
    }
    size = canvas.height

    const x = chosenSquare[0];
    const y = chosenSquare[1];
    const xw = size / xCount;
    const yw = size / yCount;
    const p = canvas.getContext("2d");

    p.beginPath();
    p.lineWidth = 2;
    p.strokeStyle = 'red';
    p.moveTo(x * xw, y * yw);
    p.lineTo((x + 1) * xw, y * yw);
    p.lineTo((x + 1) * xw, (y + 1) * yw);
    p.lineTo(x * xw, (y + 1) * yw);
    p.lineTo(x * xw, y * yw);

    p.stroke()
}

function textForCode(code) {
    switch(code) {
        case 0: return "";
        case 1: return "A";
        case 2: return "B";
        default: return "?";
    }
}

function onClick(x, y, xw, yw) {
    const xp = Math.floor(x / xw);
    const yp = Math.floor(y / yw);

    if (xp >= 0 && xp < xCount && yp >= 0 && yp < yCount) {
        const myCode = gameJson["players"]["1"]["secretId"] ? 1 : 2;
        const board = gameJson["board"];

        if (chosenSquare === null) {
            if (board[yp][xp] === myCode) {
                // Choose this piece
                chosenSquare = [xp, yp];
            }
        } else {
            if (board[yp][xp] === 0) {
                // Move to this place

                const newBoard = board.map(row => row.slice());

                const xDist = Math.abs(xp - chosenSquare[0]);
                const yDist = Math.abs(yp - chosenSquare[1]);

                if (xDist <= 1 && yDist <= 1) {
                    newBoard[chosenSquare[1]][chosenSquare[0]] = 0;
                    newBoard[yp][xp] = myCode;
                } else if (
                    (xDist === 2 || xDist == 0) &&
                    (yDist === 2 || yDist == 0)
                ) {
                    const xMid = (xp + chosenSquare[0]) / 2;
                    const yMid = (yp + chosenSquare[1]) / 2;
                    newBoard[chosenSquare[1]][chosenSquare[0]] = 0;
                    if (newBoard[yMid][xMid] !== myCode) {
                        newBoard[yMid][xMid] = 0;
                    }
                    newBoard[yp][xp] = myCode;
                }
                chosenSquare = null;
                sendMove(newBoard);
            } else {
                chosenSquare = null;
            }
        }
        renderGame(gameJson);
    }
}

function sendMove(board) {
    socket.send(JSON.stringify(
        { "board": board }
    ));
}
