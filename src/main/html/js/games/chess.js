
var socket = null
var currentLetter = null
var currentNumber = null

const characters = {
    "king" : "\u{265A}",
    "queen" : "\u{265B}",
    "bishop" : "\u{265D}",
    "knight" : "\u{265E}",
    "rook" : "\u{265C}",
    "pawn" : "\u{265F}"
}

window.onload = function() {
    makeUid()
    startWebSocket()
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }
}

function startWebSocket() {
    socket = new WebSocket(window.location.href.replace("http", "ws") + "/" + getUid());

    socket.onopen = function(event) {
        console.log("Connection established");
        socket.send("{}");
    }

    socket.onclose = function() {
        console.log("Lost Connection, Reconnecting...")
        socket = null
        setTimeout(startWebSocket, 5000)
    }

    socket.onmessage = function(event) {
        let json = JSON.parse(event.data)
        if (json["errorMessage"] == null) {
            gameJson = json
            currentLetter = null
            currentNumber = null
            renderJson(json)
        } else {
            showErrorMessage(json["errorMessage"])
        }
    }
}

function showErrorMessage(message) {
    let errorDiv = document.getElementById("errorDiv")

    errorDiv.innerHTML = message
    errorDiv.style.visibility = "visible"

    setTimeout(function() {
        errorDiv.style.visibility = "hidden"
    }, 3000)
}

function renderJson(json) {
    if (userIsNotAPlayer(json) && gameIsNotFull(json)) {
        renderLoginForm(document.getElementById("mainDiv"))
    }
    else if (gameIsNotFull(json)) {
        mainDiv = document.getElementById("mainDiv").innerHTML = "Waiting for other player...";
    }
    else {
        renderGame(json)
    }
}

function renderGame(json) {
    window.onresize = function() {
        if (document.activeElement === document.body) {
            renderJson(gameJson)
        }
    }

    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)

    size = Math.min(vw, vh)
    document.getElementById("mainDiv").innerHTML = `
        <canvas id="board" width="${size}" height="${size}" style="border: ${size/25}px solid saddlebrown;"></canvas>

    `
    canvas = document.getElementById("board")
    canvas.onmouseup = function(event) {
        const rect = canvas.getBoundingClientRect()
        const x = event.clientX - rect.left
        const y = event.clientY - rect.top
        sendMove(x, y, canvas.height / 8)
    }

    drawBoard(canvas, json)
    drawPieces(canvas, json)
}

function drawBoard(canvas, json) {
    p = canvas.getContext("2d")
    p.save()
    p.beginPath()

    p.fillStyle = "white"

    p.rect(
        0, 
        0,
        canvas.width, 
        canvas.height
    )
    
    p.fill()

    p.beginPath()
    
    p.fillStyle = "black"

    gridWidth = canvas.width / 8
    for (i = 0; i < 8; i++) {
        for (j = 0; j < 8; j++) {
            if ((j + i) % 2 == isWhite(json)) {
                p.rect(
                    i * gridWidth, 
                    j * gridWidth,
                    gridWidth, 
                    gridWidth
                )
            }
        }
    }
    p.fill()
    p.restore()
}

function drawPieces(canvas, json) {

    gridWidth = canvas.width / 8

    f = canvas.getContext("2d")
    f.save()
    f.font = `bold ${gridWidth}px Arial`
    f.textBaseline = 'middle'
    f.textAlign = 'center'
    f.lineWidth = gridWidth / 25
    
    board = json["board"]

    for ( keyX in board ) {
        x = "ABCDEFGH".indexOf(keyX)
        for ( keyY in board[keyX] ) {
            piece = board[keyX][keyY]
            if (piece) {
                f.beginPath()
                y = parseInt(keyY) - 1
                if (!isWhite(json)) {
                    y = 7 - y
                }
                f.fillStyle = piece.colour
                if (keyX == currentLetter && keyY == currentNumber) {
                    f.strokeStyle = "aqua"
                } else {
                    f.strokeStyle = piece.colour == "white" ? "black" : "white"
                }
                f.strokeText(
                    characters[piece.name],
                    (x + 0.5) * gridWidth,
                    (y + 0.5) * gridWidth
                )
                f.fillText(
                    characters[piece.name],
                    (x + 0.5) * gridWidth,
                    (y + 0.5) * gridWidth
                )
                f.stroke()
                f.fill()
            }
        }
    }

    f.restore()
}

function sendMove(x, y, gridWidth) {
    letter = "ABCDEFGH"[Math.floor(x / gridWidth)]
    number = Math.floor((y / gridWidth) + 1)
    if (!isWhite(gameJson)) {
        number = 9 - number
    }
    square = gameJson["board"][letter][number]
    if ( currentLetter && currentNumber ) {
        if ( currentLetter != letter || currentNumber != number ) {
            moveJson = {}
            moveJson[currentLetter] = {}
            moveJson[currentLetter][currentNumber] = null
            if ( currentLetter != letter ) {
                moveJson[letter] = {}
            }
            moveJson[letter][number] = gameJson["board"][currentLetter][currentNumber]
            socket.send(JSON.stringify({ board: moveJson }))
        }
        currentLetter = null
        currentNumber = null
        renderGame(gameJson)
    }
    else {
        if ( square ) {
            currentLetter = letter
            currentNumber = number
            renderGame(gameJson)
        }
    }
}

function isWhite(json) {
    return myIndex(json) == 1
}
