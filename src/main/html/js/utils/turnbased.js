function currentPlayer(json) {
    const id = (parseInt(json["turn"]) % Object.keys(json["players"]).length) + 1
    return json["players"][id.toString()]["name"]
}