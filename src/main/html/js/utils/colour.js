var colourDivVisibility = true

function renderColourDiv(json) {

    if (document.getElementById("colourDiv") == null) {
        var colourDiv = document.createElement('div');
        colourDiv.id = "colourDiv"
        document.body.appendChild(colourDiv);
    }

    colour = getColour(myIndex(json), json)

    if (colourDivVisibility) {
        document.getElementById("colourDiv").innerHTML =
            '<button onclick="toggleColourDiv()" id="liteButton">&#8250;</button>' +
            'Colour: <input type="color" id="colourSelection" value="' + colour + '" onchange="sendColour()">'
    } else {
        document.getElementById("colourDiv").innerHTML =
            '<button onclick="toggleColourDiv()" id="liteButton">&#8249;</button>'
    }
}

function sendColour() {
    index = myIndex(gameJson)
    socket.send(
        JSON.stringify(
            { 
                colours : {
                    [index.toString()] : document.getElementById("colourSelection").value
                } 
            }
        )
    ) 
}

function getColour(index, json) {
    colour = "#000000"
    try {
        colour = json["colours"][index.toString()]

        if (colour == null) {
            colour = "#000000"
        }
    } finally {
        return colour
    }

}

function toggleColourDiv() {
    colourDivVisibility = !colourDivVisibility
    renderColourDiv(gameJson)
}
