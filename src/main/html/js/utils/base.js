
function makeUid() {
    if (document.cookie.indexOf("TurnTableUid=") < 0 ) {
        const uint32 = window.crypto.getRandomValues(new Uint32Array(1))[0];
        document.cookie = "TurnTableUid=" + uint32.toString(16);
    }
}

function getUid() {
    var name = "TurnTableUid=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}