
function userIsNotAPlayer(json) {
    for (player in json["players"]) {
        if (json["players"][player]["secretId"] == getUid()) {
            return false
        }
    }
    return true
}

function gameIsNotFull(json) {
    for (player in json["players"]) {
        if (json["players"][player]["name"] == null) {
            return true
        }
    }
    return false
}

function renderLoginForm(div) {
    div.innerHTML =
        '<label id="name-label" for="name"> \
            <b>Name: </b> \
        </label> \
        <br/> \
        <input id="name" type="text"> \
        </br> \
        <button onClick=doLogin()>Submit</button>';

}

function doLogin() {
    nameBox = document.getElementById("name").value;
    for (playerId in gameJson["players"]) {
        if (gameJson["players"][playerId]["name"] == null) {
            let update = {
                players : {
                    [playerId] : {
                        name : nameBox,
                        secretId : getUid()
                    }
                }
            }
            socket.send(
                JSON.stringify(update)
            )
            return
        }
    }
}

function otherPlayerName(json) {
    for (player in json["players"]) {
        if (json["players"][player]["secretId"] == null) {
            return json["players"][player]["name"]
        }
    }

}

function myName(json) {
    for (player in json["players"]) {
        if (json["players"][player]["secretId"] != null) {
            return json["players"][player]["name"]
        }
    }
}

function playerNames(json) {
    let list = []
    for (player in gameJson["players"]) {
        list.push(json["players"][player]["name"])
    }
    return list
}

function myIndex(json) {
    for (player in json["players"]) {
        if (json["players"][player]["secretId"] != null) {
            return player
        }
    }
}

function playerIndex(name, json) {
    for (player in json["players"]) {
        if (json["players"][player]["name"] == name) {
            return player
        }
    }
}