
function gridCoordinates(canvas, event, json) {

    rect = canvas.getBoundingClientRect()

    let eventX, eventY

    if (event.type.startsWith("mouse")) {
        eventX = event.clientX
        eventY = event.clientY
    } else
    if (event.type.startsWith("touch")) {
        eventX = event.touches[0].clientX
        eventY = event.touches[0].clientY
    }

    x = eventX - rect.left
    y = eventY - rect.top

    if ( "size" in json ) {
        json["height"] = json["size"]
        json["width"] = json["size"]
    }

    size = canvas.height
    gridCount = calculateGridSize(json)
    gridSize = size / gridCount

    gridX = Math.ceil(x / gridSize) - (gridCount - json["width"])
    gridY = Math.ceil(y / gridSize) - (gridCount - json["height"])

    gridX = Math.min(gridX, gridCount - 1)
    gridY = Math.min(gridY, gridCount - 1)

    if (gridX < 0 || gridY < 0) {
        return [-1, -1]
    }

    return [gridX, gridY]
}

function calculateGridSize(json) {
    return Math.max(json["height"], json["width"]) + Math.max(biggestArray(json["topClues"]), biggestArray(json["leftClues"]))
}

function biggestArray(json) {
    maxLength = 1
    for (key in json) {
        maxLength = json[key].length > maxLength ? json[key].length : maxLength
    }
    return maxLength
}