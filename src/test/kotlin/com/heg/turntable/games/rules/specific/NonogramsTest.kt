package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class NonogramsTest {

    private val mapper = ObjectMapper()

    @Test
    fun initialise_map_creates_a_grid_of_correct_size() {
        val nonogram = Nonograms(mapper)
        val initialJson = nonogram.initialiseMap(
            mapper.readTree("""{"height": "8", "width": "9"}""").deepCopy()
        )
        for (i in 1..9) {
            for (j in 1..8) {
                assertTrue(initialJson["grid"][i.toString()].has(j.toString()))
            }
            assertFalse(initialJson["grid"][i.toString()].has("9"))
        }
        assertFalse(initialJson["grid"].has("10"))
    }

    @Test
    fun setting_newNonogram_to_true_generates_a_new_nonogram_and_resets_grid() {
        val nonogram = Nonograms(mapper)
        val originalJson = nonogram.initialiseMap(
            mapper.readTree("""{"height": "10", "width": "10"}""").deepCopy()
        )
        val newJson = nonogram.alterWrite(
            originalJson,
            mapper.readTree("""{ "newNonogram": true }""").deepCopy(),
            "blah"
        )
        assertTrue(newJson.has("grid"))
        assertTrue(newJson.has("topClues"))
        assertTrue(newJson.has("leftClues"))
        assertFalse(newJson.has("newNonogram"))
    }

}