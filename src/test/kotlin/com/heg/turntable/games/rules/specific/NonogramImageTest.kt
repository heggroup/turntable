package com.heg.turntable.games.rules.specific

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NonogramImageTest {

    private val mapper = ObjectMapper()

    @Test
    fun nonogram_image_is_correct_size_when_initialised() {
        val img = NonogramImage(
            17,
            10,
            mapper
        )
        assertEquals(17, img.generateLeftNumbers().size())
        assertEquals(10, img.generateTopNumbers().size())
    }

    @Test
    fun nonogram_clues_have_the_same_totals() {
        val img = NonogramImage(
            8,
            8,
            mapper,
            listOf(
                listOf(true, true, false, true, false, false, true, true),
                listOf(true, true, false, true, false, true, true, false),
                listOf(false, true, false, true, false, false, true, true),
                listOf(true, true, false, true, false, false, true, true),
                listOf(true, false, false, true, false, false, true, true),
                listOf(true, true, false, true, false, true, true, true),
                listOf(false, false, false, true, false, false, true, true),
                listOf(true, true, false, true, false, false, true, true)
            )
        )

        fun total_clues(node: ObjectNode): Int {
            var total = 0
            node.fields().forEach { row -> row.value.forEach{ total += it.asInt() } }
            return total
        }

        assertEquals(
            total_clues(img.generateTopNumbers()),
            total_clues(img.generateLeftNumbers())
        )
    }

}