package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.webserver.WebSocketException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ReadyUpTest {

    val mapper = ObjectMapper()

    @Test
    fun ready_up_writes_are_accepted() {
        assertEquals(
            mapper.readTree("""{ "readys" : { "1" : true } }"""),
            ReadyUp(mapper, "key").alterWrite(
                mapper.readTree("""
                    {
                        "readys" : {
                            "1" : false,
                            "2" : false
                        },
                        "players" : {
                            "1" : {
                                "secretId" : "id"
                            }
                        }
                    }"""),
                mapper.readTree("""{"readys":{"1":true}}""").deepCopy(),
                "id"
            )
        )
    }

    @Test
    fun ready_up_writes_for_other_players_are_rejected() {
        try {
            ReadyUp(mapper, "key").alterWrite(
                mapper.readTree("""
                    {
                        "readys" : {
                            "1" : false,
                            "2" : false
                        },
                        "players" : {
                            "1" : {
                                "secretId" : "id"
                            },
                            "2" : {
                                "secretId" : "id2"
                            }
                        }
                    }"""),
                mapper.readTree("""{"readys":{"1":true}}""").deepCopy(),
                "id2"
            )
            fail("No exception thrown")
        } catch (e: WebSocketException) {

        }
    }

}