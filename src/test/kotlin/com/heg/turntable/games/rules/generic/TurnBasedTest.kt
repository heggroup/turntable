package com.heg.turntable.games.rules.generic

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.webserver.WebSocketException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class TurnBasedTest {

    private val mapper: ObjectMapper = ObjectMapper()

    @Test
    fun turn_and_protected_namespace_are_added_to_map_when_initialised() {
        assertEquals(
            mapper.readTree("""{"turn" : 0, "turnBasedArea" : null}"""),
            TurnBased("turnBasedArea").initialiseMap(
                mapper.readTree("{}").deepCopy()
            )
        )
    }

    @Test
    fun if_it_is_not_your_turn_and_you_try_to_write_in_the_area_a_401_is_returned() {
        try {
            TurnBased().alterWrite(
                mapper.readTree("""
                    {
                        "turn" : 0,
                        "board" : "unchanged",
                        "players": {
                            "1" : { "secretId" : "player1" },
                            "2" : { "secretId" : "player2" }
                        }
                    }
                    """),
                mapper.readTree("""{"board" : "changed"}""").deepCopy(),
                "player2"
            )
            fail("No Error thrown")
        } catch (e : WebSocketException) {
            assertEquals("It is not your turn", e.errorMessage)
        }
    }

    @Test
    fun if_it_is_not_your_turn_and_you_try_to_write_outside_the_area_a_401_is_not_returned() {
        assertEquals(
            mapper.readTree("""{"offboard" : "changed"}"""),
            TurnBased().alterWrite(
                mapper.readTree("""
                    {
                        "turn" : 0,
                        "board" : "unchanged",
                        "players": {
                            "1" : { "secretId" : "player1" },
                            "2" : { "secretId" : "player2" }
                        }
                    }
                    """),
                mapper.readTree("""{"offboard" : "changed"}""").deepCopy(),
                "player2"
            )
        )
    }

    @Test
    fun if_it_is_your_turn_json_is_returned_with_turn_incremented() {
        assertEquals(
            mapper.readTree("""{"board" : "changed", "turn": 2}"""),
            TurnBased().alterWrite(
                mapper.readTree("""
                    {
                        "turn" : 1,
                        "board" : "unchanged",
                        "players": {
                            "1" : { "secretId" : "player1" },
                            "2" : { "secretId" : "player2" }
                        }
                    }
                    """),
                mapper.readTree("""{"board" : "changed"}""").deepCopy(),
                "player2"
            )
        )
    }

    @Test
    fun if_it_is_your_turn_and_you_change_outside_the_area_json_is_returned_without_turn_incremented() {
        assertEquals(
            mapper.readTree("""{"offboard" : "changed"}"""),
            TurnBased().alterWrite(
                mapper.readTree("""
                    {
                        "turn" : 1,
                        "board" : "unchanged",
                        "players" : { 
                            "1" : { "secretId" : "player1" },
                            "2" : { "secretId" : "player2" }
                        }
                    }
                    """),
                mapper.readTree("""{"offboard" : "changed"}""").deepCopy(),
                "player2"
            )
        )
    }
}