package com.heg.turntable.games

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.datasource.FakeDataStore
import com.heg.turntable.datasource.MergingJsonStore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import java.util.concurrent.locks.ReentrantLock

internal class MoveExecutorTest {

    private val mapper = ObjectMapper()

    @Test
    fun first_move_is_executed_immediately() {

        val executor = MoveExecutor(
            MergingJsonStore(
                FakeDataStore()
            ),
            GameRegistry(
                mapper,
                emptyMap()
            )
        )

        executor.write(
            """{"move":1}""",
            "user1",
            "ABCD",
            "game"
        )

        assertEquals(
            mapper.readTree("""{ "move" : 1 }"""),
            executor.read("ABCD", "game", "whocares")
        )

    }

    @Test
    fun move_is_not_executed_until_lock_is_released() {

        val lock = ReentrantLock(true)

        val executor = MoveExecutor(
            MergingJsonStore(
                FakeDataStore()
            ),
            GameRegistry(
                mapper,
                emptyMap()
            ),
            StringMultiLock(
                lockMap = mutableMapOf(
                    "ABCD" to lock
                )
            )
        )

        lock.lock()

        GlobalScope.launch {
            executor.write(
                """{"move":1}""",
                "user1",
                "ABCD",
                "game"
            )
        }

        // TODO: Feel ashamed
        while ( !lock.hasQueuedThreads() )
            Thread.yield()

        assertEquals(
            mapper.readTree("""{}"""),
            executor.read("ABCD", "game", "whocares")
        )

        lock.unlock()
        lock.lock()

        assertEquals(
            mapper.readTree("""{ "move" : 1 }"""),
            executor.read("ABCD", "game", "whocares")
        )

    }

    @Test
    fun lock_is_deleted_after_all_jobs_are_executed() {

        val lockMap = mutableMapOf("ABCD" to ReentrantLock(true))

        val executor = MoveExecutor(
            MergingJsonStore(
                FakeDataStore()
            ),
            GameRegistry(
                mapper,
                emptyMap()
            ),
            StringMultiLock(lockMap = lockMap)
        )

            executor.write(
                """{"move":"blah"}""",
                "user1",
                "ABCD",
                "game"
            )

        assertFalse(
            lockMap.containsKey("ABCD")
        )

    }
}