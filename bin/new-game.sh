#!/bin/bash

echo What would you like your game to be called?

read -r GAMENAME

if [[ "$GAMENAME" == "*.*" ]]; then
  echo Game name cannot contain a period! Aborting...
  exit 1
fi

echo Initialising html directory...

FILENAME=$(echo $GAMENAME | tr " " "-" | tr '[:upper:]' '[:lower:]')

mkdir "src/main/html/$FILENAME"
cp "src/main/html/template/index.html" "src/main/html/$FILENAME/"
cp "src/main/html/template/thumb.html" "src/main/html/$FILENAME/"
cp "src/main/html/template/form.html" "src/main/html/$FILENAME/"
cp "src/main/html/template/example.png" "src/main/html/$FILENAME/"
cp "src/main/html/template/script.js" "src/main/html/js/games/${FILENAME}.js"

for FILE in ./src/main/html/$FILENAME/*; do
  sed -i "s/GAMENAME/$GAMENAME/g" "$FILE"
  sed -i "s/FILENAME/$FILENAME/g" "$FILE"
done
